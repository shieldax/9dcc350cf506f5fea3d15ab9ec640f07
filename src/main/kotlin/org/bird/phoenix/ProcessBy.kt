package org.bird.phoenix

import com.sun.jna.Platform
import org.bird.phoenix.windows.Windows

fun processByID(processID: Int): Process? = when {
    Platform.isWindows() || Platform.isWindowsCE() -> Windows.openProcess(processID)
    else -> null
}

fun processByName(processName: String): Process? = when {
    Platform.isWindows() || Platform.isWindowsCE() -> Windows.openProcess(processName)
    else -> null
}